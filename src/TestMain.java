
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeMap;

public class TestMain {

	private static final int MAXROWLENGTH = 81;
	private static int totalweight = 0;

	public static TreeMap<Integer, ArrayList<String>> parseWords() throws IOException {
		TreeMap<Integer, ArrayList<String>> allWords = new TreeMap<Integer, ArrayList<String>>();
		BufferedReader br = new BufferedReader(new FileReader("alastalon_salissa.txt"));
		String line;
		while ((line = br.readLine()) != null) {
			if (line.length() > 0) {
				for (String word : Arrays.asList(line.trim().split("\\s+"))) {
					int key = word.length() + 1;
					totalweight += key;
					if (allWords.containsKey(key)) {
						allWords.get(key).add(word + " ");
					} else {
						ArrayList<String> words = new ArrayList<>();
						words.add(word + " ");
						allWords.put(key, words);
					}
				}
			}
		}
		totalweight++; // ++ for surplus row
		br.close();
		return allWords;
	}

	public static String getWord(TreeMap<Integer, ArrayList<String>> allWords, int key) {
		if (key == -1) {
			key = allWords.lastKey();
		}
		String word = allWords.get(key).get(0);
		allWords.get(key).remove(0);
		if (allWords.get(key).isEmpty()) {
			allWords.remove(key);
		}
		return word;
	}

	public static ArrayList<Integer> wordsAsInt(TreeMap<Integer, ArrayList<String>> allWords, int hiKey) {
		ArrayList<Integer> keys = new ArrayList<Integer>();
		Set<Integer> keySet = allWords.headMap(hiKey).keySet();
		for (Integer key : keySet) {
			keys.addAll(Collections.nCopies(allWords.get(key).size(), key));
		}
		return keys;
	}

	public static void main(String[] args) throws IOException {
		// Start timer
		long startTime = System.currentTimeMillis();

		// Get all words in multimap
		TreeMap<Integer, ArrayList<String>> allWords = parseWords();

		ArrayList<String> finishedRows = new ArrayList<String>();
		ArrayList<String> rows = new ArrayList<String>();

		do {
			String word = getWord(allWords, -1);
			int wordLength = word.length();
			boolean wordPlaced = false;
			for (int i = 0; i < rows.size(); i++) {
				String row = rows.get(i);
				int rowLength = row.length();
				int freeSpaceNow = MAXROWLENGTH - rowLength;
				if (freeSpaceNow >= wordLength) {
					int freeSpaceAfter = freeSpaceNow - wordLength;
					if (allWords.containsKey(freeSpaceAfter)) {
						String newRow = row + word + getWord(allWords, freeSpaceAfter);
						rows.set(i, newRow);
						wordPlaced = true;
						if (rows.get(i).length() == MAXROWLENGTH) {
							finishedRows.add(rows.get(i));
							rows.remove(i);
						}
						break;
					} else if (allWords.isEmpty() || freeSpaceAfter > allWords.firstKey()) {
						String newRow;
						// need refactoring help here!
						if (!allWords.isEmpty() && allWords.lastKey() == 4 && freeSpaceAfter == 9) {
							newRow = row + word + getWord(allWords, 3);
						} else {
							newRow = row + word;
						}
						rows.set(i, newRow);
						wordPlaced = true;
						if (rows.get(i).length() == MAXROWLENGTH) {
							finishedRows.add(rows.get(i));
							rows.remove(i);
						}
						break;
					}
				}
			}
			if (!wordPlaced)
				rows.add(word);

		} while (!allWords.isEmpty());

		FileWriter fw = new FileWriter("newBook.txt");
		for (String row : finishedRows) {
			fw.write(row.trim() + "\r");
		}
		for (String row : rows) {
			fw.write(row.trim() + "\r");
		}
		fw.close();

		// Stop timer
		long elapsedTime = (System.currentTimeMillis() - startTime);

		System.out.println("total weight / container volume = \"the perfect case\"");
		System.out.println(totalweight + " / " + MAXROWLENGTH + " = " + totalweight / MAXROWLENGTH);
		System.out.println("");
		System.out.println("total n of containers used: " + (rows.size() + finishedRows.size()));
		System.out.println("full filled containers: " + finishedRows.size());
		System.out.println("partially filled containers: " + rows.size());
		System.out.println("");
		// Print execution time
		System.out.println("time: " + elapsedTime + "ms");
	}
}